import { Server } from "socket.io"
import readline from "readline"

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

const END = "END"

const port = process.env.PORT || 6969
const io = new Server(port)

const users = {}

console.log(`Running on: ${port}`)
io.on('connection', socket => {
  socket.on('new-user', name => {
    users[socket.id] = name
    socket.broadcast.emit('user-connected', name)
    console.log(`New user connected: ${name}`)
  })

  socket.on('send-chat-message', message => {
    socket.broadcast.emit('chat-message', { message: message, name: users[socket.id] })
    console.log(`[${users[socket.id]}]: ${message}`)
  })

  socket.on('user-disconnect', () => {
    socket.broadcast.emit('user-disconnected', users[socket.id])
    console.log(`User ${users[socket.id]} disconnected`)
    delete users[socket.id]
  })

})

rl.on("line", line => {
  if (line===END){
    process.exit(0)
  }
})
