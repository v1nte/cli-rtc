import { io } from "socket.io-client"
import esMain from "es-main"
import readline from "readline"

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

const END = "END"

const connect = (host, port) => {
  console.log(`Connecting to ${host} on ${port}`)
  const socket = io(`http://${host}:${port}`)

  rl.question("Type your name: ", (name) => {
    socket.emit("new-user", name)
  })

  rl.on("line", message =>{
    if (message===END) {
      socket.emit("user-disconnect")
      process.exit(0)
    }
    socket.emit('send-chat-message', message)
  })

  socket.on("chat-message", data => {
    console.log(`[${data.name}]: ${data.message}`)
  })

  socket.on("user-connected", name => {
    console.log(`${name} Just joined`)
  })

  socket.on("user-disconnected", name => {
    console.log(`${name} disconnected`)
  })
}

const main = () => {
  const HOST = process.env.HOSTIO || 'localhost'
  const PORT = process.env.PORT || 6969

  if (isNaN(PORT)) {
    console.error("invalid port", PORT)
    process.exit(1)
  }

  const port = Number(PORT)

  connect(HOST, port)
}

if (esMain(import.meta)) {
  main()
}
